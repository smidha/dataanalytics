package textanalytics;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.ResourceBundle;

import javax.xml.ws.Response;
import org.apache.http.conn.ClientConnectionRequest;
import org.codehaus.jackson.map.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.microtripit.mandrillapp.lutung.MandrillApi;
import com.textmagic.sdk.RequestMethod;
import com.textmagic.sdk.RestClient;
import com.textmagic.sdk.RestException;
import com.textmagic.sdk.RestResponse;
import com.textmagic.sdk.resource.instance.*;
import com.textmagic.sms.TextMagicMessageService;

public class TextMagicConfiguration {
private String username;
private String apiKey;
private String endPoint;
private String statsPath;
private String startDate;	
	public TextMagicConfiguration(String textMagicConfigFile) throws RestException
 {
	
		     ResourceBundle resourceBundle = ResourceBundle.getBundle(textMagicConfigFile);
	       this.apiKey=resourceBundle.getString("apiKey");
	       this.username=resourceBundle.getString("username");
	       this.endPoint=resourceBundle.getString("endPoint");
	       this.statsPath=resourceBundle.getString("statsPath");
	       this.startDate=resourceBundle.getString("startDate");
	        
	 
 }
 public JsonObject getTextAnalytics() throws RestException
 {//	 TextMagicMessageService service=new TextMagicMessageService("yverma@dkidirect.com","Password#1");
	 //https://rest.textmagic.com/api/v2   
	 Map<String,String> params=new HashMap<String, String>();
	 params.put("start",this.startDate);
	 RestClient client=new RestClient(this.username,this.apiKey,this.endPoint);
	 String response=client.request(statsPath,RequestMethod.GET,params).getJsonResponse();
   	// System.out.println("Text Analytics Response String:"+response);
	 String responseArray[]=response.split(",");
	 JsonObject textData=new JsonObject();
	    
	 for(int i=0;i<responseArray.length;i++)
	 {
		 if(responseArray[i].contains(("messagesSentDelivered")))
				 
				 {
			 textData.addProperty("deliveredTexts",responseArray[i].substring(responseArray[i].indexOf(":")+1));
				 }

		 else if(responseArray[i].contains(("messagesSentFailed")))
				 {
			 textData.addProperty("failedTexts",responseArray[i].substring(responseArray[i].indexOf(":")+1));
				 }

		 else if(responseArray[i].contains(("messagesSentRejected")))
				 {
			 textData.addProperty("rejectedTexts",responseArray[i].substring(responseArray[i].indexOf(":")+1));
				 }

		 else if(responseArray[i].contains(("messagesSentParts")))
				 {
			 textData.addProperty("sentTexts",responseArray[i].substring(responseArray[i].indexOf(":")+1));
				 }
	 }
	return textData;

 }
 }

