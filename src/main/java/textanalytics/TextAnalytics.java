package textanalytics;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.JsonObject;
import com.textmagic.sdk.RestException;

import textanalytics.TextMagicConfiguration;

/**
 * Servlet implementation class TextAnalytics
 */
public class TextAnalytics extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TextAnalytics() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter pw=null;
		try {
		TextMagicConfiguration textMagicConfig=new TextMagicConfiguration("resources.TextMagicConfiguration");
	JsonObject textData=textMagicConfig.getTextAnalytics();
	
		 response.setContentType("application/json");
		 pw=response.getWriter();
		    pw.print(textData);
		    
		    
	} catch (RestException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
		finally {
			pw.flush();
			pw.close();

		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
