package emailanalytics;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.concurrent.locks.ReentrantLock;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.JsonArray;

/**
 * Servlet implementation class EnrollmentAnalytics
 */
public class EnrollmentAnalytics extends HttpServlet {
	  
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EnrollmentAnalytics() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
	
	DBConfiguration.configureDB("resources.DBConfiguration");
	Connection conn = null;
	ResultSet rs = null;
	PreparedStatement ps=null;
	PrintWriter pw=null;
    try {
		Class.forName(DBConfiguration.driver);
	} catch (ClassNotFoundException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
    
    ArrayList<MonthlyEnrollment> enrollementList=new ArrayList<MonthlyEnrollment>();
       //connect to DB
   try {
	   conn = DriverManager.getConnection(DBConfiguration.url,DBConfiguration.username,DBConfiguration.password);
 ps=conn.prepareStatement("select count(*),MONTH(DATEADD(second, created_on/1000,'19700101')),YEAR(DATEADD(second, created_on/1000,'19700101')) from "+DBConfiguration.table+" where tenant_uid=? AND is_active=? and is_deleted=? and Roles=? group BY MONTH(DATEADD(second, created_on/1000,'19700101')),YEAR(DATEADD(second, created_on/1000,'19700101')) order by YEAR(DATEADD(second, created_on/1000,'19700101')),MONTH(DATEADD(second, created_on/1000,'19700101'))");

	ps.setString(1,DBConfiguration.tenantuid);
	ps.setInt(2, DBConfiguration.isActive);
	ps.setInt(3,DBConfiguration.isDeleted);
	ps.setString(4,DBConfiguration.roles);
	
	rs=ps.executeQuery();
      while(rs.next())
    {
    	
    //	System.out.println("Printing enrollment data:"+rs.getInt(1)+" "+rs.getInt(2)+"-"+rs.getInt(3));
    	MonthlyEnrollment monthlyEnrollment=new MonthlyEnrollment();
    	monthlyEnrollment.setCount(rs.getInt(1));
    	monthlyEnrollment.setMonth(rs.getInt(2));
    	monthlyEnrollment.setYear(rs.getInt(3));
    	
    		enrollementList.add(monthlyEnrollment);
    		monthlyEnrollment=null;
    	}
	
    
      String jsonEnrollmentList = new Gson().toJson(enrollementList);
     // System.out.println(jsonEnrollmentList);
      response.setContentType("application/json");
    pw=response.getWriter();
    pw.print(jsonEnrollmentList);
      
   } catch (SQLException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}                
     finally
     {
    	 try {
			conn.close();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	 try {
			rs.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	 
    try {
		ps.close();
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
    
    
    pw.flush();
    pw.close();
     }
   
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
