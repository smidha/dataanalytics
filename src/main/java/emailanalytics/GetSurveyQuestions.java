package emailanalytics;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.JsonObject;

/**
 * Servlet implementation class GetSurveyQuestions
 */
public class GetSurveyQuestions extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetSurveyQuestions() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		DBConfiguration.configureDB("resources.DBConfiguration");
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement ps=null;
		PrintWriter pw=null;
		  JsonObject questionsData=new JsonObject();
		  response.setContentType("application/json");
		  
		    
		try {
			Class.forName(DBConfiguration.driver);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    try {
	    	pw=response.getWriter();
	  	  
	 	   conn = DriverManager.getConnection(DBConfiguration.url,DBConfiguration.username,DBConfiguration.password);
	  ps=conn.prepareStatement("select distinct code,question from "+DBConfiguration.questionsTable+" where code like ('STCQ%') and code not in ('STCQ1','STCQ23') order by code");
	  rs=ps.executeQuery();
      while(rs.next())
      {
    	 questionsData.addProperty(rs.getString(1),rs.getString(2));
      }
      //send survey data to front end
        pw.println(questionsData);
        
	  }
	    catch(Exception e)
	    {
	    	System.out.print("Exception:"+e);
	    }
	    
	    finally
	    {
	    	try {
	    		  {rs.close();}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    
	    	try {
				ps.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    	try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    
	    	pw.close();
	    	pw.flush();
	    }
	}


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
