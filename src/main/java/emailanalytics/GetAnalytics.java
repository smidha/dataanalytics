package emailanalytics;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.microtripit.mandrillapp.lutung.model.MandrillApiError;
import com.microtripit.mandrillapp.lutung.view.MandrillSender;

/**
 * Servlet implementation class GetAnalytics
 */
public class GetAnalytics extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetAnalytics() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		MandrillConfiguration mandrillConfiguration=new MandrillConfiguration("resources.MandrillConfiguration");
		  PrintWriter pw=null;
		try {
			MandrillSender mandrillSender=mandrillConfiguration.setAndGetMandrillSender("info@stc.com");
		    JsonObject emailData=new JsonObject();
		    
		    emailData.addProperty("sent",mandrillConfiguration.getSent());
		    emailData.addProperty("hardBounce",mandrillConfiguration.getHardBounces());
		    emailData.addProperty("softBounce",mandrillConfiguration.getSoftBounces());
		    emailData.addProperty("opened",mandrillConfiguration.getUniqueOpens());
		    emailData.addProperty("uniqueClicked",mandrillConfiguration.getUniqueClicks());
		    emailData.addProperty("clicked", mandrillConfiguration.getClicks());
		    emailData.addProperty("rejects",mandrillConfiguration.getRejects());
		    emailData.addProperty("delivered", mandrillConfiguration.getDelivered());
		    emailData.addProperty("optOuts",mandrillConfiguration.getOptOuts());
		    emailData.addProperty("videoClicks",mandrillConfiguration.getVideoClicks()[0]);
		    emailData.addProperty("uniqueVideoClicks",mandrillConfiguration.getVideoClicks()[1]);
		    response.setContentType("application/json");
		    pw=response.getWriter();
		    pw.print(emailData);
		    
	    } catch (MandrillApiError e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally
		{
		
			pw.flush();
			pw.close();    
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
