package emailanalytics;

import java.io.IOException;
import java.util.ResourceBundle;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.microtripit.mandrillapp.lutung.MandrillApi;
import com.microtripit.mandrillapp.lutung.controller.MandrillSendersApi;
import com.microtripit.mandrillapp.lutung.model.MandrillApiError;
import com.microtripit.mandrillapp.lutung.view.MandrillSender;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class MandrillConfiguration {
	private String apiKey;
    private String senderAddress;
    private MandrillSender mandrillSender;
    private String mandrillFileLocation;
    private MandrillApi mandrillApi;
    private MandrillSendersApi mandrillSendersApi;
    private String queryVideoLink;
    
   public MandrillConfiguration(String mandrillFileLocation)
   {
	   this.mandrillFileLocation=mandrillFileLocation;
	   ResourceBundle resourceBundle = ResourceBundle.getBundle(mandrillFileLocation);
       this.apiKey=resourceBundle.getString("apiKey");
       this.senderAddress=resourceBundle.getString("senderAddress");
       this.queryVideoLink=resourceBundle.getString("queryVideoLink");
        mandrillApi=new MandrillApi(apiKey);
       
   }
   public MandrillSender setAndGetMandrillSender(String senderAddress) throws MandrillApiError, IOException
   {
	   if(this.senderAddress==null)
	   {
		   this.senderAddress=senderAddress;
	   }
	   this.mandrillSendersApi=mandrillApi.senders();
	   MandrillSender[] mandrillSenderArr=mandrillSendersApi.list();
	   for(int i=0;i<mandrillSenderArr.length;i++)
	   {
		   mandrillSender = mandrillSenderArr[i];
           
           if(mandrillSender.getAddress().equals(this.senderAddress))
               break;
              
	   }
	   
	   return mandrillSender;
   }
   
   public int getClicks()
   {
	   
	   return mandrillSender.getClicks();
	 
   }

   public int getDelivered()
   {
	   return (mandrillSender.getSent()-mandrillSender.getRejects()-mandrillSender.getHardBounces()-mandrillSender.getSoftBounces());
   }
   public int getOpens()
   {
	   return mandrillSender.getOpens();
   }
   public int getSent()
   {
	   return mandrillSender.getSent();
   }
   public int getOptOuts()
   {
	   return mandrillSender.getUnsubs();
   }
   public int getUniqueClicks()
   {
	   return mandrillSender.getUniqueClicks();
   }
   public int getUniqueOpens()
   {
	   return mandrillSender.getUniqueOpens();
   }
   public int getHardBounces()
   {
	   return mandrillSender.getHardBounces();
   }
   public int getSoftBounces()
   {
	   return mandrillSender.getSoftBounces();
   }
public int getRejects()
{
	return mandrillSender.getRejects();
}
public String[] getVideoClicks(){
	Client client = Client.create();

	WebResource webResource = client.resource("https://mandrillapp.com/api/1.0/urls/search");
	JsonObject input=new JsonObject();
	input.addProperty("key",this.apiKey);
	input.addProperty("q",this.queryVideoLink);
	
	ClientResponse response = webResource.type("application/json").post(ClientResponse.class, input.toString());

			if (response.getStatus() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
				     + response.getStatus());
			}

		    String output = response.getEntity(String.class);
			JsonParser parser = new JsonParser();	
			JsonObject jsonOutput =parser.parse(((output.replace("[","")).replace("]","")).trim()).getAsJsonObject();
			String videoClicks= jsonOutput.get("clicks").getAsString();
			String uniqueVideoClicks=jsonOutput.get("unique_clicks").getAsString();
			String[] videoClicksSummary={videoClicks,uniqueVideoClicks};
			return videoClicksSummary;
}
}
