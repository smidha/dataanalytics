<html>
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <script src="http://code.jquery.com/jquery-1.9.1.js" type="text/javascript"></script>
    <script src="http://code.highcharts.com/highcharts.js" type="text/javascript"></script>
    <script src="http://code.highcharts.com/modules/exporting.js" type="text/javascript"></script>
   <script src="config.js"></script>
   <link href='https://fonts.googleapis.com/css?family=Abel' rel='stylesheet' type='text/css'>
    <script type="text/javascript">
	var sent=0;
	var clicked=0;
	var opened=0;
	var softBounce=0;
	var hardBounce=0;
	var delivered=0;
	var optOuts=0;
    var rejects=0;
    var uniqueClicked=0;
    var videoClicks=0;
    var uniqueVideoClicks=0;
    $(document).ready(function(){

	$.get(qOptions.emailAnalyticsURL, function(data,status){
		//alert("Data: " + data + "\nStatus: " + status);
	
		var json = data;
		
		 sent=parseInt($.trim(json["sent"]),10);
		 clicked=parseInt($.trim(json["clicked"]),10);
		 uniqueClicked=parseInt($.trim(json["uniqueClicked"]),10);
		 opened=parseInt($.trim(json["opened"]),10);
		 softBounce=parseInt($.trim(json["softBounce"]),10);
		 hardBounce=parseInt($.trim(json["hardBounce"]),10);
		 rejects=parseInt($.trim(json["rejects"]),10);
		 delivered=parseInt($.trim(json["delivered"]),10);
		 optOuts=parseInt($.trim(json["optOuts"]),10);
		 videoClicks=parseInt($.trim(json["videoClicks"]),10);
		 uniqueVideoClicks=parseInt($.trim(json["uniqueVideoClicks"]),10);
		 emailOpenRateBarGraph(opened,delivered);
		 emailClickThroughRateBarGraph(clicked,delivered);
		 emailOptOutRateBarGraph(optOuts,delivered);
		drawPieChart(sent,delivered,softBounce,hardBounce,rejects);	
		setSentEmail(sent);
		setDeliveredEmail(delivered);
		setVideoMetrics(videoClicks,uniqueVideoClicks);
	});
	
    });
    $(document).ready(function(){
      	$.get(qOptions.enrollmentAnalyticsURL, function(data,status){
      	 var json=data;
      	 var totalEnrollments=0;
      	 $.each(json, function(key) {
 			totalEnrollments+=json[key].count;
      	});
      	 	$("#totalEnrollmentsLabel").text(totalEnrollments);
      		drawEnrollmentBarGraph(json);
      		
      	});
      });  
    
    $(document).ready(function(){
      	$.get(qOptions.textAnalyticsURL, function(data,status){

      var json=data;
	 var sentTexts=parseInt($.trim(json["sentTexts"]),10);
	 var rejectedTexts=parseInt($.trim(json["rejectedTexts"]),10);
	 var deliveredTexts=parseInt($.trim(json["deliveredTexts"]),10);
	 var failedTexts=parseInt($.trim(json["failedTexts"]),10);
		$("#sentTextsLabel").text(sentTexts);
		$("#deliveredTextsLabel").text(deliveredTexts);
		$("#rejectedTextsLabel").text(rejectedTexts);
		$("#failedTextsLabel").text(failedTexts);
	    
      	});
      });  
    
    $(document).ready(function(){
    	  var questionsCodeArray = [];
          var responseArray=[];
          var questionsArray=[];
        
    	$.get(qOptions.surveyAnalyticsURL, function(data,status){
      // build the index
       var json=data;
       $.each(json, function(i, n){
       questionsCodeArray.push(i);
        responseArray.push(n);
              });
      	
    	$.get(qOptions.surveyQuestionsURL, function(data,status){
    	          var questionsJson=data;
    	         var index=0;
    		$.each(questionsJson, function(i, n){
    			
    			   questionsArray.push(n);
    			   drawDonutChart(i,n,responseArray[index]);
    			   index++;
    			   
    			           });
    		
    	});
    	
    	
    	});	    	      
      	
    });
    
    
    function drawEnrollmentBarGraph(json)
    {
 		
 		var countSeries = [];
 		var monthSeries=[];
 		var yearSeries=[];
 		var timeSeries=[];
 		var monthInWordsSeries=[];
 		$.each(json, function(key) {
 			countSeries.push(json[key].count);
 			monthSeries.push(json[key].month);
 			monthInWordsSeries.push(monthInWordFormat(json[key].month));
 			yearSeries.push(json[key].year);
 			timeSeries.push(monthInWordFormat(json[key].month)+"-"+json[key].year);
 			});
 		
    	    $('#EnrollmentContainer').highcharts({
    	        chart: {
    	            type: 'column'
    	        },
    	        title: {
    	            text: 'Monthly Enrollment'
    	        },
				credits: {
				enabled: false
				}, 	     
    	        xAxis: {
    	            categories:timeSeries,
    	            crosshair: true
    	        },
    	        yAxis: {
    	            min: 0,
    	            title: {
    	                text: 'No. of Enrollments'
    	            }
    	        },
    	        tooltip: {
    	            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
    	            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
    	                '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
    	            footerFormat: '</table>',
    	            shared: true,
    	            useHTML: true
    	        },
    	        plotOptions: {
    	            column: {
    	                pointPadding: 0.2,
    	                borderWidth: 0,
    	                colorByPoint:true
    	            }
    	        },
    	        series: [{
    	            name: 'Monthly Enrollment',
    	            data: countSeries

    	        }]
    	    });
    	      
  }

   function  monthInWordFormat(monthNumber)
	{
	   var monthInWords;
    if(monthNumber==1)
    	{
    	monthInWords="Jan";
    	}
    else if(monthNumber==2)
    {
    	monthInWords="Feb";
    }
    else if(monthNumber==3)
    {
    	monthInWords="Mar";
    }else if(monthNumber==4)
    {
    	monthInWords="Apr";
    }else if(monthNumber==5)
    {
    	monthInWords="May";
    }
    else if(monthNumber==6)
    {
    	monthInWords="Jun";
    }
    else if(monthNumber==7)
    {
    	monthInWords="Jul";
    }
    else if(monthNumber==8)
    {
    	monthInWords="Aug";
    }
    else if(monthNumber==9)
    {
    	monthInWords="Sep";
    }else if(monthNumber==10)
    {
    	monthInWords="Oct";
    }else if(monthNumber==11)
    {
    	monthInWords="Nov";
    }
    else if(monthNumber==12)
    {
    	monthInWords="Dec";
    }
    
    return monthInWords;
	}
   
   
   
         function setSentEmail(sent)
    {
    	$("#totalSentLabel").text(sent);
    }
    function setDeliveredEmail(delivered)
    {
    	$("#totalDeliveredLabel").text(delivered);
    }
   
    
    function emailOpenRateBarGraph(opened,delivered)
    {
    	var tempOpened=(opened/delivered)*100;
    	 $('#OpenRateContainer').highcharts({
             chart: {
                 type: 'column'
             },
             title: {
                 text: 'Email Open Rate'
             },
             credits: {
 				enabled: false
 				},
            
             xAxis: {
                 categories: [
                     'April',
                    ],
                 crosshair: true
             },
             yAxis: {
                 min: 0,
                 title: {
                     text: 'Percentage(%)'
                 }
             },
             tooltip: {
                 headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                 pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                     '<td style="padding:0"><b>{point.y:.1f} </b></td></tr>',
                 footerFormat: '</table>',
                 shared: true,
                 useHTML: true
             },
             plotOptions: {
                 column: {
                     pointPadding: 0.2,
                     borderWidth: 0
                 }
             },
             series: [{
                 name: '%',
                 data: [tempOpened]
             }]
         });
  
    }

    function emailOptOutRateBarGraph(optOuts,delivered)
    {
    	var tempOptOuts=(optOuts/delivered)*100;
    	 $('#OptOutRateContainer').highcharts({
             chart: {
                 type: 'column'
             },
             title: {
                 text: 'Email OptOut Rate'
             },
             credits: {
 				enabled: false
 				},
           
             xAxis: {
                 categories: [
                     'April',
                    ],
                 crosshair: true
             },
             yAxis: {
                 min: 0,
                 title: {
                     text: 'Percentage(%)'
                 }
             },
             tooltip: {
                 headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                 pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                     '<td style="padding:0"><b>{point.y:.1f} </b></td></tr>',
                 footerFormat: '</table>',
                 shared: true,
                 useHTML: true
             },
             plotOptions: {
                 column: {
                     pointPadding: 0.2,
                     borderWidth: 0
                 }
             },
             series: [{
                 name: '%',
                 data: [tempOptOuts]
             }]
         });
  
    }
    
   
    function emailClickThroughRateBarGraph(clicked,delivered)
    {
    	var tempClicked=(clicked/delivered)*100;
    	 $('#ClickRateContainer').highcharts({
             chart: {
                 type: 'column'
             },
             title: {
                 text: 'Email Click Through Rate'
            
             },
             credits: {
 				enabled: false
 				},
             xAxis: {
                 categories: [
                     'April',
                    ],
                 crosshair: true
             },
             yAxis: {
                 min: 0,
                 title: {
                     text: 'Percentage(%)'
                 }
             },
             tooltip: {
                 headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                 pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                     '<td style="padding:0"><b>{point.y:.1f} </b></td></tr>',
                 footerFormat: '</table>',
                 shared: true,
                 useHTML: true
             },
             plotOptions: {
                 column: {
                     pointPadding: 0.2,
                     borderWidth: 0
                 }
             },
             series: [{
                 name: '%',
                 data: [tempClicked]
             }]
         });
  
    }
  
    
        
	function drawPieChart(total,delivered,softBounce,hardBounce,rejects) {
		var tempDelivered=(delivered/total)*100;
		var tempSoftBounce=(softBounce/total)*100;
		var tempHardBounce=(hardBounce/total)*100;
		var tempRejects=(rejects/total)*100;
		$('#PieContainer').highcharts({
	        chart: {
	            plotBackgroundColor: null,
	            plotBorderWidth: null,
	            plotShadow: false,
	            type: 'pie'
	        },
	        title: {
	            text: 'Sent Emails Status '
	        },
	        tooltip: {
	            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
	        },
	        plotOptions: {
	            pie: {
	                allowPointSelect: true,
	                cursor: 'pointer',
	                dataLabels: {
	                    enabled: true,
	                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
	                    style: {
	                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
	                    }
	                }
	            }
	        },
	        series: [{
	            name: 'EmailEvents',
	            colorByPoint: true,
	            data: [ 
	              {
	                name: 'delivered',
	                y: tempDelivered
	            }, {
	                name: 'Soft Bounce',
	                y: tempSoftBounce
	            }, {
	                name: 'Hard Bounce',
	                y: tempHardBounce
	            },
	            {
	                name: 'Rejects',
	                y: tempRejects
	            }
	            ]
	        }]
	    });
	}
	
	function setVideoMetrics(clicked,uniqueClicked)
	{
		$("#clickedLabel").text(clicked);
		$("#uniqueClickedLabel").text(uniqueClicked);
		   
	}
	function drawDonutChart(questionCode,question,response)
	{
			var total=5;
			
			var value=response.toString();
			if(value.indexOf('.')<0)
				{
				value=value+'.0';
				}
		    // Create the chart
	         chart = new Highcharts.Chart({
	            chart: {
	                renderTo: $.trim(questionCode+'Container'),
	               type: 'pie',
	            	   spacingBottom:0 ,
	                   spacingTop: 0,
	                   spacingLeft: 0,
	                   spacingRight: 0,

	                 },
	            
	            title: {
	            	style:{
	            		color: 'rgba(49,153,213,1)',
	                    fontWeight: 'bold',
	                    fontSize: '300%',
	                    fontFamily: 'Abel'
	            	},
	                verticalAlign: 'middle',
	                floating: true,
	                text: value
	            },
	                         credits: {
 				enabled: false
 				},
 				colors: ['rgba(49,153,213,1)','rgba(205,205,205,1)'],
 	           
 				 yAxis: {
	                title: {
	                    text: 'Average Response Rating'
	                }
	            },
	            plotOptions: {
	                pie: {
	                    shadow: false
	                }
	            },
	            tooltip: {
	                formatter: function() {
	                    return '<b>'+ this.point.name +'</b>: '+ this.y ;
	                }
	            },
	            series: [{
	                name: 'Survey Response Average Rating',
	              
	                data: [["Average Rating",response],["Max - Average Rating",total-response]],
	                size: '40%',
	                innerSize: '70%',
	               // showInLegend:true,
	                dataLabels: {
	                    enabled: false
	                }
	            }]
	        });
	    	    $("#"+questionCode+"Label").text(question);
   
	 
	  }

	   </script>
	   </head>
	   <body>
	   <h1>Program Metrics</h1>
	   <table align="center" width="100%" cellpadding="15" bgcolor="white"  style="table-layout:fixed;">
	   <tr><td>
	    <label  style="color:black;font-family:Abel;font-size:200%;"> Enrollments</label><br>
	   </td></tr>
	  <tr>
	   	<td style="text-align:center;">
	   	    <label  style="color:black;font-family:Abel;font-size:100%;">TOTAL ENROLLMENTS</label><br>
	        <label style="color:#38E9F7;font-family:Abel;font-size:200%" id="totalEnrollmentsLabel"></label>	 
	   	</td> 
	   <td>
	  	    <div id="EnrollmentContainer" style=" margin: 0 auto"></div>  
	  </td>
	  <tr><td>
	  	  <label  style="color:black;font-family:Abel;font-size:200%;"> Email Engagement</label><br>
	  </td></tr>
	<tr>
		<td align="center">
			<label style="color:black;font-family:Abel;font-size:100%">Total Sent</label> <br/>
	  <label style="color:#38E9F7;font-family:Abel;font-size:200%" id="totalSentLabel"></label>
	  <hr width="50%">
	  <label style="color:gray;font-family:Abel;font-size:90%">Total Delivered</label>
	  &nbsp;
	  <label style="color:gray;font-family:Abel;font-size:90%" id="totalDeliveredLabel"></label>
	  
		</td>
			
		<td align="center">
		<div id="OpenRateContainer" style=" margin: 0 auto"></div>
		</td>
	</tr>
		<tr>
			<td align="center">
			<div id="ClickRateContainer" style="margin: 0 auto"></div>
			</td>
			<td align="center">
			 <div id="OptOutRateContainer" style=" margin: 0 auto"></div>
	  
			</td>
		</tr>
		<tr><td>
		  	  <label  style="color:black;font-family:Abel;font-size:200%;"> Text Messages Metrics</label><br>	
		</td></tr>	
		<tr>
			<td align="center">
			     <label style="color:black;font-family:Abel;font-size:100%">TOTAL SENT TEXTS</label><br> 
	   				<label style="color:#38E9F7;font-family:Abel;font-size:200%" id="sentTextsLabel"></label>
			</td>
			<td align="center">
			     <label style="color:black;font-family:Abel;font-size:100%">TOTAL DELIVERED TEXTS</label><br> 
	   				<label style="color:#38E9F7;font-family:Abel;font-size:200%" id="deliveredTextsLabel"></label>
			</td>
		</tr>
		<tr>
			<td align="center">
			 <label style="color:black;font-family:Abel;font-size:100%">TOTAL REJECTED TEXTS</label><br>
			 <label style="color:#38E9F7;font-family:Abel;font-size:200%" id="rejectedTextsLabel"></label>
			</td>
			<td align="center">
			<label style="color:black;font-family:Abel;font-size:100%">TOTAL FAILED TEXTS</label><br>
				   <label style="color:#38E9F7;font-family:Abel;font-size:200%" id="failedTextsLabel"></label>
			
			</td>
		</tr>
		<tr>
				<td>
				 <label  style="color:black;font-family:Abel;font-size:200%;"> Video Metrics</label><br>	
				</td>
		</tr>		
		<tr>
				<td align="center"> 
			    <label style="color:black;font-family:Abel;font-size:100%">TOTAL CLICKS</label><br>
				<label style="color:#38E9F7;font-family:Abel;font-size:200%" id="clickedLabel"></label>
				</td>
				<td align="center">
				<label style="color:black;font-family:Abel;font-size:100%">TOTAL UNIQUE CLICKS</label><br>
				<label style="color:#38E9F7;font-family:Abel;font-size:200%" id="uniqueClickedLabel"></label>
				</td>
		</tr>
		<tr>
			<td>
				<label  style="color:black;font-family:Abel;font-size:200%;"> Survey Results</label><br>	
			</td>
		</tr>
		<tr>
			<td>
		    	<label id="STCQ2Label" style="color:black;font-family:Abel;font-size:100%">STCQ2 Response</label><br>
				<div id="STCQ2Container" style=" margin: 0 auto" align="center"></div>
			</td>
			<td>
		    	<label id="STCQ3Label" style="color:black;font-family:Abel;font-size:100%">STCQ3 Response</label><br>
				<div id="STCQ3Container" style=" margin: 0 auto" align="center"></div>
			</td>
			<td>
		    	<label id="STCQ4Label" style="color:black;font-family:Abel;font-size:100%">STCQ4 Response</label><br>
				<div id="STCQ4Container" style=" margin: 0 auto" align="center"></div>
			</td>
		</tr>
		<tr>
			<td>
		    	<label id="STCQ5Label" style="color:black;font-family:Abel;font-size:100%">STCQ5 Response</label><br>
				<div id="STCQ5Container" style=" margin: 0 auto" align="center"></div>
			</td>
		
		
			<td>
		    	<label id="STCQ6Label" style="color:black;font-family:Abel;font-size:100%">STCQ6 Response</label><br>
				<div id="STCQ6Container" style=" margin: 0 auto" align="center"></div>
			</td>
			<td>
		    	<label id="STCQ7Label" style="color:black;font-family:Abel;font-size:100%">STCQ7 Response</label><br>
				<div id="STCQ7Container" style=" margin: 0 auto" align="center"></div>
			</td>
		</tr>
		<tr>
			<td>
		    	<label id="STCQ8Label" style="color:black;font-family:Abel;font-size:100%">STCQ8 Response</label><br>
				<div id="STCQ8Container" style=" margin: 0 auto" align="center"></div>
			</td>
			<td>
		    	<label id="STCQ9Label" style="color:black;font-family:Abel;font-size:100%">STC9 Response</label><br>
				<div id="STCQ9Container" style=" margin: 0 auto" align="center"></div>
			</td>
		
			<td>
		    	<label id="STCQ10Label" style="color:black;font-family:Abel;font-size:100%">STCQ10 Response</label><br>
				<div id="STCQ10Container" style=" margin: 0 auto" align="center"></div>
			</td>
		</tr>
		<tr>
			<td>
		    	<label id="STCQ11Label" style="color:black;font-family:Abel;font-size:100%">STCQ11 Response</label><br>
				<div id="STCQ11Container" style=" margin: 0 auto" align="center"></div>
			</td>
			<td>
		    	<label id="STCQ12Label" style="color:black;font-family:Abel;font-size:100%">STCQ12 Response</label><br>
				<div id="STCQ12Container" style=" margin: 0 auto" align="center"></div>
			</td>
			<td>
		   		 <label id="STCQ13Label" style="color:black;font-family:Abel;font-size:100%">STCQ13 Response</label><br>
				<div id="STCQ13Container" style=" margin: 0 auto" align="center"></div>
			</td>
		</tr>
		<tr>
			<td>
		    	<label id="STCQ14Label" style="color:black;font-family:Abel;font-size:100%">STCQ14 Response</label><br>
				<div id="STCQ14Container" style=" margin: 0 auto" align="center"></div>
			</td>
			<td>
		    	<label id="STCQ15Label" style="color:black;font-family:Abel;font-size:100%">STCQ15 Response</label><br>
				<div id="STCQ15Container" style=" margin: 0 auto" align="center"></div>
			</td>
			<td>
		    	<label id="STCQ16Label" style="color:black;font-family:Abel;font-size:100%">STCQ16 Response</label><br>
				<div id="STCQ16Container" style=" margin: 0 auto" align="center"></div>
			</td>
		</tr>
		<tr>	
			<td>
		    	<label id="STCQ17Label" style="color:black;font-family:Abel;font-size:100%">STCQ17 Response</label><br>
				<div id="STCQ17Container" style=" margin: 0 auto" align="center"></div>
			</td>
			<td>
			    <label id="STCQ18Label" style="color:black;font-family:Abel;font-size:100%">STCQ18 Response</label><br>
				<div id="STCQ18Container" style=" margin: 0 auto" align="center"></div>
			</td>
			<td>
		    	<label id="STCQ19Label" style="color:black;font-family:Abel;font-size:100%">STCQ19 Response</label><br>
				<div id="STCQ19Container" style=" margin: 0 auto" align="center"></div>
			</td>
		</tr>
		<tr>	
			<td>
		    	<label id="STCQ20Label" style="color:black;font-family:Abel;font-size:100%">STCQ20 Response</label><br>
				<div id="STCQ20Container" style=" margin: 0 auto" align="center"></div>
			</td>
			<td>
		    	<label id="STCQ21Label" style="color:black;font-family:Abel;font-size:100%">STCQ21 Response</label><br>
				<div id="STCQ21Container" style=" margin: 0 auto" align="center"></div>
			</td>
			<td>
		    	<label id="STCQ22Label" style="color:black;font-family:Abel;font-size:100%">STCQ22 Response</label><br>
				<div id="STCQ22Container" style=" margin: 0 auto" align="center"></div>
			</td>
		</tr>
	
			</table>
	
	
	     <!-- <tr><td>
	      <div id="AllRatesContainer" style="min-width: 400px; height: 400px; margin: 0 auto"></div>
	  </td></tr> -->
	   </body>
	 </html>